# Arbot Core Arch Repository
The core software repository for Arbot Linux.

# How To Use
1. Add this to `/etc/pacman.conf`:
```
[arbot-core-repo]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/ArbotLinux/$repo/-/raw/main/$arch
```

2. Run this command:
```
sudo pacman -Syy
```